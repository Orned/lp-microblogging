# main.py

from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import login_required, current_user

from .models import Post
from .models import User
from . import db

main = Blueprint('main', __name__)

@main.route('/')
def home():
    return render_template('index.html')

@main.route('/posts')
def index():
    return render_template('posts/index.html', posts = Post.query.all())

@main.route("/posts/new")
def new():
    if current_user.is_authenticated:
        return render_template('posts/new.html')
    else:
        flash("You must be authenticated to perform this action")
        return redirect(url_for("auth.login"))

@main.route('/posts/<id>/edit')
def edit(id = None):
    if current_user.is_authenticated:
        return render_template('posts/edit.html', post = Post.query.filter_by(id = id).first())
    else:
        flash("You must be authenticated to perform this action")
        return redirect(url_for("auth.login"))

@main.route('/posts/<id>/destroy')
def destroy(id = None):
    if current_user.is_authenticated:
        post = Post.query.filter_by(id = id).first()

        db.session.delete(post)
        db.session.commit()

        return redirect(url_for("main.index"))
    else:
        flash("You must be authenticated to perform this action")
        return redirect(url_for("auth.login"))

@main.route('/posts/<id>', methods=['POST'])
def update(id = None):
    if current_user.is_authenticated:
        post = Post.query.filter_by(id = id).first()

        post.title = request.form.get('title')
        post.body = request.form.get('body')

        db.session.commit()

        return redirect(url_for('main.index'))
    else:
        flash("You must be authenticated to perform this action")
        return redirect(url_for("auth.login"))

@main.route("/posts/create", methods=['POST'])
def create():
    if current_user.is_authenticated:
        title = request.form.get('title')
        body = request.form.get('body')
        user_id = current_user.id

        new_post = Post(title = title, body = body, user_id = user_id)

        db.session.add(new_post)
        db.session.commit()

        return redirect(url_for('main.index'))
    else:
        flash("You must be authenticated to perform this action")
        return redirect(url_for("auth.login")) 

@main.route("/users/<id>/posts")
def users_posts(id = None):
    user = User.query.filter_by(id = id).first()
    posts = user.post

    return render_template('users/posts.html', posts = posts, user = user)

@main.route("/user")
def users():
    users = User.query.all()

    return render_template('users/index.html', users = users)