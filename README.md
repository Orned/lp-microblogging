
## Pour le lancement de l'application

```bash
pipenv install
pipenv shell
export FLASK_APP=__init__.py
python -m flask run
```

## Si probleme avec la base de donnée

Supprimer le fichier db.sqlite puis dans le shell de l'env :
```bash
flask initdb
```